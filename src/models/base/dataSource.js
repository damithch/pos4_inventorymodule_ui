class DataSource {

    find(where, select = null) {
        //let payload;
        let message;
        return new Promise((resolve, reject) => {
            db.find({
                selector: where,
                fields: select
            }).then(function (response) {
                message = {
                    "code": 200,
                    "title": "Success",
                    "message": "Find Success"
                }
                // payload = new util.payload(1, message, response.docs);
                // resolve(payload.generatePayload());
            }).catch(function (error) {
                message = {
                    "code": 300,
                    "title": "Internal DB Error",
                    "message": error
                };
                // payload = new util.payload(0, message, []);
                // reject(payload.generatePayload());
            });
        });
    };

    save(data) {
        //let payload;
        let message;
        return new Promise((resolve, reject) => {
            db.put(data)
                .then(response => {
                    message = {
                        "code": 200,
                        "title": "Success",
                        "message": "Successfully Saved"
                    }
                    // payload = new util.payload(1, message, response);
                    // resolve(payload.generatePayload());
                })
                .catch((error) => {
                    message = {
                        "code": 300,
                        "title": "Internal DB Error",
                        "message": error
                    };
                    // payload = new util.payload(0, message, []);
                    // reject(payload.generatePayload());
                });
        });
    };

    get(documentType) {
        //let payload;
        let message;
        return new Promise((resolve, reject) => {
            db.get(documentType)
                .then(function (response) {
                    message = {
                        "code": 200,
                        "title": "Success",
                        "message": "Find Success"
                    }
                    // payload = new util.payload(1, message, response);
                    // resolve(payload.generatePayload());
                }).catch(function (error) {
                    console.log("DOC TYPE: " + JSON.stringify(error));
                    message = {
                        "code": 300,
                        "title": "Internal DB Error",
                        "message": error
                    };
                    // payload = new util.payload(0, message, []);
                    // reject(payload.generatePayload());
                });
        });
    }

}

module.exports = DataSource;