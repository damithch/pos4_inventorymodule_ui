import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import PurchaseOrder from '@/components/PurchaseOrder'
import PoList from '@/components/PoList'
import ItemPopup from '@/components/ItemPopup'
import Summary from '@/components/Summary'
import GrnList from '@/components/GrnList'
import GRN from '@/components/GRN'
import GrnSummary from '@/components/GrnSummary'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PoList',
      component: PoList
    },

    {
      path: '/PurchaseOrder',
      name: 'PurchaseOrder',
      component: PurchaseOrder
    },
    {
      path: '/PoList',
      name: 'PoList',
      component: PoList
    },
    {
      path: '/ItemPopup',
      name: 'ItemPopup',
      component: ItemPopup
    },
    {
      path: '/Summary',
      name: 'Summary',
      component: Summary
    },
    {
      path: '/GrnList',
      name: 'GrnList',
      component: GrnList
    },
    {
      path: '/GRN',
      name: 'GRN',
      component: GRN
    },
    {
      path: '/GrnSummary',
      name: 'GrnSummary',
      component: GrnSummary
    }
  ]
})
